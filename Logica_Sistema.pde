import mqtt.*;
import processing.data.JSONObject;
import processing.sound.*;

SoundFile alarm_sound;
MQTTClient client;

String tema2 = "glud";

boolean opened = false;
boolean away = false;
float changer = 0;
int last_change_time = 0;
int interval = 5000;
PImage opened_door;
PImage closed_door;
Boton alarm_button; 

void setup() {
  size(450, 400);
  background(255,255,255);
  textSize(20);
  last_change_time = millis();
  client = new MQTTClient(this);
  client.connect("mqtt://public:public@public.cloud.shiftr.io", "processing");
  client.subscribe(tema2);
  closed_door = loadImage("../Media/closed.png");
  opened_door = loadImage("../Media/opened.png");
  alarm_sound = new SoundFile(this, "../Media/alarm-clock-short-6402.wav");
  alarm_button = new Boton(140, 30, 180, 20, "Fuera de casa", 255, 0, 0);
}

void draw() {
  background(255, 255, 255);
  if (away && opened && !alarm_sound.isPlaying()) {
    alarm_sound.play();
  }
  
  if (!away && alarm_sound.isPlaying()) {
    alarm_sound.stop();
  }

  if (opened) {
    image(opened_door, 100, 100, opened_door.width / 2, opened_door.height / 2);
  } else {
    image(closed_door, 100, 100, closed_door.width / 2, closed_door.height / 2);
  }

  alarm_button.update();
  alarm_button.render();

  if (alarm_button.isClicked()) {
    away = !away;

    if (away) {
      alarm_button.setText("Fuera de casa");
    } else {
      alarm_button.setText("En casa");
    }
    alarm_button.resetClick();
  }
  printText();
}

void printText(){
  text("Usted se encuentra:", 230, 10);
  if (opened){
    text("PUERTA ABIERTA", 220, 350);
    fill(0);
  }
  else{
    text("PUERTA CERRADA", 220, 350);
    fill(0);
  }
}

void messageReceived(String topic, byte[] payload) {
  String payloadString = new String(payload);
  println("Dato recibido en /" + topic + ": " + payloadString);

  int colonIndex = payloadString.indexOf(':');

  if (colonIndex != -1 && colonIndex < payloadString.length() - 1) {
    String valueString = payloadString.substring(colonIndex + 1).trim();

    opened = Boolean.parseBoolean(valueString);

    println("Puerta abierta: " + opened);
  } else {
    println("Formato de payload no válido");
  }
}
