import mqtt.*;
import processing.serial.*;

Serial myPort;
MQTTClient client;
String data;

String tema = "glud";

void setup() {
  //size(200, 200);
  client = new MQTTClient(this);
  client.connect("mqtt://public:public@public.cloud.shiftr.io", "processing");
  client.subscribe(tema);
  String portName = Serial.list()[2];
  println("Puertos disponibles:");
  printArray(Serial.list());
  println("Puerto conectado: " + portName);
  println("--------------------------");
  myPort = new Serial(this, portName, 9600);
}

void draw() {
  if ( myPort.available() > 0) {
    data = myPort.readStringUntil(10);
    if(data != null){
      print("Dato Arduino: " + data);
      client.publish(tema, data);
      print("Dato publicado en /" + tema + ": " + data);
    }
  }
}

void messageReceived(String topic, byte[] payload) {
  println("Dato recibido en /" + topic + ": " + new String(payload));
}
